# Release Notes Base Version

## API & Master Sync

- Masters& Transaction API to other ERP (Standard API for integration)
- Master auto sync once in day at first login.
- On Demand Sync option for Import/Export/Image posting
- On Demand DB back up Sync option.(Auto Local DB and posting to FTP server)
- Manual apk download.

## Image Posting and Web View

- LPO/Sign Images to FTP and View the images in Web
- Sign images can be viewed in Customer mail for Order/Invoices

## Configuration & Settings

- Application activate/Deactivate handling from back end
- Modules (Order, Collection, Invoice, etc...) enable/Disable.
- Modules (Order, Collection, Invoice, etc...) – Remove feature.
- Colour - Theme Change (Based on requirement)
- Logo change based on company setup – hardcoded for now.

## Features

- Secondary master Edit option in TAB
- Secondary master Credit Limit/Credit Days features is captured
- Customer additional information capture.
- Restrict the Invoice using credit limit Check
- Collection status update in back office and revert back the status to tab
- Stock adjustments feature
- Price default and can edit if required in Stock receipt and Stock adjustment
- Expense flow with approval by back office and status update to tab
- Sales target setup - Customer visit, Total Sales of products with Qty, Value, Count
- Stock batch details view for the warehouse.
- Multi UOM selection on manual orders.
- Copy Order in order module
- Edit Template in order module
- Copy Invoice in Invoice module
- Edit Template in Invoice module
- Trip Number Reset in Mobile using admin.
- Forgot password
- Reset password
- Multiple SMS option on Order and Invoice Transactions
- Meeting enhancement
    - Schedule meeting
    - View Meeting
    - Reschedule meeting
    - Meeting conflict
    - Cancel meeting
    - Complete meeting

- primary customer creation from tab 

- primary and secondary customer creation with approval flow from back end 

- transaction restriction until approved from back end 

- customer classification in both primary and secondary 

- doctor visit changed to hcp  

- hcp classification on creation 

- order approval flow with 2 levels 

- price list update on primary and secondary approval from back edn 

- gps capture 

- only tab version 
